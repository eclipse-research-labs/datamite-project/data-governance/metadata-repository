# metadata-repository


# DESCRIPTION
This project will host the developments for the Metadata Repository, one of the components of the Data Governance Module, as can be seen in the architecture figure below.

![architecture_DGB](docs/figures/arch_extended_MetRep.png)

The Metadata Repository is the technology used to store the metadata from the different datasets that have been uploaded or identified in the framework. This technology is not developed within the project, but adopted. The technology selected to perform this role is Apache Atlas. This project will include the files needed for the correct deployment and integration of the metadata repository.

The metadata repository will store metadata according to the following schema.
![metadata_schema](docs/figures/metadata_schema.png)

Both figures can be found in the [docs/figures](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-governance/metadata-repository/-/tree/main/docs/figures) folder.

## INSTALLATION & REQUIREMENTS

The Metadata Repository will be provided as a set of containers with the different technologies required to be deployed with Apache Atlas. 

## ROADMAP

- [x] Selection of the technology to use.
- [x] Creation of the entities from the metadata schema in the metadata repository.
- [ ] Integration with other components.
- [ ] Automation of the deployment.
- [ ] Integration with Data Security Module

## LICENSE
In principle this component is distributed under Apache license.
